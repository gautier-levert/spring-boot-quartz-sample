package test.api;

import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import test.job.SampleJob;

import static org.quartz.DateBuilder.futureDate;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.JobKey.jobKey;
import static org.quartz.TriggerBuilder.newTrigger;
import static org.quartz.TriggerKey.triggerKey;

@Component
@RestController
public class SampleController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SampleController.class);

    @Autowired
    private Scheduler scheduler;

    @RequestMapping("/")
    String home() {

        try {
            TriggerKey triggerKey = triggerKey("myTrigger", "test");

            if (scheduler.checkExists(triggerKey)) {
                scheduler.unscheduleJob(triggerKey);
            }

            JobKey jobKey = jobKey("myJob", "test");
            JobDetail jobDetail;
            if (scheduler.checkExists(jobKey)) {
                jobDetail = scheduler.getJobDetail(jobKey);
                jobDetail.getJobDataMap().put("sampleValue", "updatedValue");
                scheduler.addJob(jobDetail, true);
            } else {
                jobDetail = newJob(SampleJob.class)
                        .storeDurably()
                        .withIdentity(jobKey)
                        .usingJobData("sampleValue", "originalValue")
                        .build();

                scheduler.addJob(jobDetail, false);
            }

            Trigger trigger = newTrigger()
                    .withIdentity(triggerKey)
                    .forJob(jobDetail)
                    .startAt(futureDate(10, DateBuilder.IntervalUnit.SECOND))
                    .usingJobData("sampleValue", "triggerValue")
                    .build();

            scheduler.scheduleJob(trigger);

        } catch (SchedulerException e) {
            LOGGER.error("", e);
        }

        return "Hello World!";
    }

    @RequestMapping("/cancel")
    String cancel() {
        TriggerKey triggerKey = triggerKey("myTrigger", "test");

        try {
            if (scheduler.checkExists(triggerKey)) {
                scheduler.unscheduleJob(triggerKey);
            }
        } catch (SchedulerException e) {
            LOGGER.error("", e);
        }

        return "Cancel...";
    }
}
